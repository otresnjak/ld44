﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public float AttackRadius = 2.0f;
    public Vector2 AttackIntervalRange = new Vector2(8.0f, 16.0f);
    public GameObject AttackRadiusIndicator;

    void Start()
    {
        var scale = AttackRadiusIndicator.transform.localScale;
        scale.x = scale.z = AttackRadius;
        AttackRadiusIndicator.transform.localScale = scale;
        timeToNextAttack = Random.Range(AttackIntervalRange.x, AttackIntervalRange.y);
    }

    void Update()
    {
        if (timeToNextAttack <= 0.0f)
        {
            var candidates = Physics.OverlapSphere(transform.position, AttackRadius);
            foreach (var c in candidates)
            {
                Human h = c.GetComponent<Human>();
                if (h != null && h.Enemy)
                {
                    h.Splatter();
                    break;
                }
            }

            timeToNextAttack = Random.Range(AttackIntervalRange.x, AttackIntervalRange.y);
        }

        timeToNextAttack -= Time.deltaTime;
    }

    float timeToNextAttack;
}
