﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkAltar : MonoBehaviour
{
    void Awake()
    {
        b = GetComponent<Building>();
    }

    void Update()
    {
        if (b.DoneBuilding && !didTheThing)
        {
            didTheThing = true;
            var allHumans = FindObjectsOfType<Human>();
            foreach (var h in allHumans)
                h.Sacrifice(true);
            Player.Instance.Win = true;
        }
    }

    Building b;
    bool didTheThing = false;
}
