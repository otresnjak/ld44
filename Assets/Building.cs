﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    public AnimationCurve ScaleCurve;
    public float ScaleTime = 2.0f;
    public float DestructionScaleTime = 1.0f;
    public float FullScale = 1.0f;
    public float SpawnHeight = 0.1f;
    public int BuildCost = 1;
    public int HousingValue = 0;
    public int StatueValue = 0;
    public int TempleValue = 0;
    public int GraveyardValue = 0;
    public int HitPoints = 3;
    public string Name;
    public string Description;

    public bool DoneBuilding { get; set; }

    void Awake()
    {
        colliders = GetComponentsInChildren<Collider>();
        body = GetComponentInChildren<Rigidbody>();
        DoneBuilding = false;
    }

    void Start()
    {
        ApplyBonuses();
        UpdateScale();
        damageCooldownLeft = damageCooldown;
        gameObject.name = Name;
    }

    void Update()
    {
        UpdateScale();
        age += Time.deltaTime;

        if (damageCooldownLeft > 0.0f)
            damageCooldownLeft -= Time.deltaTime;

        if (HitPoints <= 0)
        {
            float t = Mathf.Clamp01(1.0f - (timeSinceDestroyed / DestructionScaleTime));
            timeSinceDestroyed += Time.deltaTime;
            float scale = ScaleCurve.Evaluate(t) * FullScale;
            transform.localScale = new Vector3(scale, scale, scale);
        }
    }

    public void Damage(Vector3 source)
    {
        if (damageCooldownLeft <= 0.0f)
        {
            HitPoints -= 1;
            damageCooldownLeft = damageCooldown;

            Vector3 nearestPt = NearestPointOnBound(source);
            Vector3 enemyToMe = ((nearestPt - source) + Vector3.up * 0.25f).normalized;

            body.AddForceAtPosition(enemyToMe * 200.0f, nearestPt);

            if (HitPoints <= 0)
                Invoke("DestroyMe", 1.0f);
        }
    }

    void UpdateScale()
    {
        float t = Mathf.Clamp01(age / ScaleTime);
        float scale = ScaleCurve.Evaluate(t) * FullScale;
        transform.localScale = new Vector3(scale, scale, scale);

        if (t >= 0.95f) DoneBuilding = true;
    }

    public void DestroyMe()
    {
        Debug.Log("Building destroyed.");
        RemoveBonuses();
        Destroy(gameObject);
    }

    public void ApplyBonuses()
    {
        Player.Instance.Housing += HousingValue;
        Player.Instance.Statues += StatueValue;
        Player.Instance.Temples += TempleValue;
        Player.Instance.Graveyards += GraveyardValue;
    }

    public void RemoveBonuses()
    {
        Player.Instance.Housing -= HousingValue;
        Player.Instance.Statues -= StatueValue;
        Player.Instance.Temples -= TempleValue;
        Player.Instance.Graveyards -= GraveyardValue;
    }

    public Vector3 NearestPointOnBound(Vector3 pt)
    {
        Vector3 closest = transform.position;
        float dist = float.MaxValue;

        foreach(var c in colliders)
        {
            var testpt = c.ClosestPointOnBounds(pt);
            if (Vector3.Distance(pt, testpt) < dist)
                closest = testpt;
        }

        return closest;
    }


    readonly float damageCooldown = 1.0f;
    float damageCooldownLeft;

    float age = 0.0f;
    float timeSinceDestroyed = 0.0f;
    Collider[] colliders;
    Rigidbody body;
}
