﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySelf : MonoBehaviour
{
    public float Delay = 5.0f;

    void Update()
    {
        if (age > Delay)
        {
            Destroy(gameObject);
        }

        age += Time.deltaTime;
    }

    float age;
}
