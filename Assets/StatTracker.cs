﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StatTracker
{
    public static Dictionary<string, int> Stats = new Dictionary<string, int>();

    public static void Clear()
    {
        Stats.Clear();
    }

    public static void IncStat(string Stat, int Amt = 1)
    {
        if (Stats.ContainsKey(Stat))
            Stats[Stat] += Amt;
        else
            Stats.Add(Stat, Amt);
    }

    public static void SetStat(string Stat, int Val)
    {
        if (Stats.ContainsKey(Stat))
            Stats[Stat] = Val;
        else
            Stats.Add(Stat, Val);
    }

    public static int GetStat(string Stat)
    {
        if (Stats.ContainsKey(Stat))
            return Stats[Stat];
        else
            return 0;
    }
}
