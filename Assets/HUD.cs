﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUD : MonoBehaviour
{
    public static HUD Instance;
    public Human SelectedHuman;
    public TMPro.TMP_Text PopulationText;
    public TMPro.TMP_Text HousingText;
    public TMPro.TMP_Text PowerText;
    public TMPro.TMP_Text SelectionInfo;
    public GameObject SelectionIndicator;
    public TMPro.TMP_Text ShortcutInfo;
    public TMPro.TMP_Text BuildingInfo;
    public TMPro.TMP_Text NextWaveText;
    public TMPro.TMP_Text EndGameText;
    public AnimationCurve VictoryTextScaleCurve;

    public GameObject MassSacrificeIndicatorPrefab;

    void Awake()
    {
        Instance = this;
    }

    void UpdateVictoryText()
    {
        timeSinceGameEnd += Time.deltaTime;
        float scale = VictoryTextFinalScale * VictoryTextScaleCurve.Evaluate(Mathf.Clamp01(timeSinceGameEnd / VictoryTextScaleTime));
        EndGameText.transform.localScale = new Vector3(scale, scale, scale);
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.Instance.Won())
        {
            EndGameText.gameObject.SetActive(true);
            EndGameText.text = "THE DARK GOD HAS RISEN\n<size=50>You win!</size>";

            UpdateVictoryText();
            if (!endingGame)
            {
                endingGame = true;
                Invoke("EndGame", 5.0f);
            }
        }
        else if (Player.Instance.Lost())
        {
            EndGameText.gameObject.SetActive(true);
            EndGameText.text = "YOUR VILLAGE IS NO MORE\n<size=50>You've lost.</size>";

            UpdateVictoryText();
            if (!endingGame)
            {
                endingGame = true;
                Invoke("EndGame", 5.0f);
            }
        }
        else
        {
            EndGameText.gameObject.SetActive(false);
        }

        PopulationText.text = "Population: " + Human.Population;
        HousingText.text = "Housing: " + Player.Instance.Housing;
        PowerText.text = "Dark Power: " + Player.Instance.Power;

        float maxNextWaveTime = 120.0f;
        float timeToNextWave = EnemyWaveSpawner.Instance.TimeToNextWave;
        if (Human.EnemyPopulation > 0)
        {
            NextWaveText.gameObject.SetActive(true);
            NextWaveText.text = string.Format("{0} knights attacking the village!", Human.EnemyPopulation);
        }
        else if (timeToNextWave >= 0.0f && timeToNextWave < maxNextWaveTime)
        {
            NextWaveText.gameObject.SetActive(true);
            NextWaveText.text = string.Format("Knights incoming in {0} seconds!", Mathf.FloorToInt(timeToNextWave));
        }
        else
        {
            NextWaveText.gameObject.SetActive(false);
        }

        if (SelectedHuman != null)
        {
            if (SelectedHuman.Enemy)
                SelectionInfo.text = string.Format("{0}\n{1}", SelectedHuman.GetName(), SelectedHuman.GetFlavorText());
            else
                SelectionInfo.text = string.Format("{0}\n{1} years old\n{2}", SelectedHuman.GetName(), SelectedHuman.GetAge().ToString(), SelectedHuman.GetFlavorText());
            SelectionIndicator.SetActive(true);
            SelectionIndicator.transform.position = SelectedHuman.transform.position + Vector3.up * 0.4f;
            ShortcutInfo.gameObject.SetActive(true);
        }
        else
        {
            SelectionInfo.text = "";
            SelectionIndicator.SetActive(false);
            ShortcutInfo.gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Q))
            Sacrifice();
        if (Input.GetKeyDown(KeyCode.E))
            Spawn();
        if (Input.GetKey(KeyCode.K))
            Kill();

        if (Input.GetKeyDown(KeyCode.M))
        {
            massSacrificeInProgress = true;
            massSacrificeHeld = 0.0f;
        }

        if (massSacrificeInProgress)
        {
            if (Input.GetKey(KeyCode.M))
            {
                if (!massSacrificeIndicator)
                    massSacrificeIndicator = Instantiate(MassSacrificeIndicatorPrefab);
                UpdateMassSacrificeIndicator();
                massSacrificeHeld += Time.deltaTime;
            }
            else
            {
                if (massSacrificeIndicator != null)
                    Destroy(massSacrificeIndicator);
                massSacrificeHeld = 0.0f;
                massSacrificeInProgress = false;
            }
        }

        if (massSacrificeHeld >= massSacrificeHoldTime)
        {
            massSacrificeHeld = 0.0f;
            massSacrificeInProgress = false;
            Destroy(massSacrificeIndicator);
            MassSacrifice();
        }

        if (Player.Instance.CurrentBuilding != null)
        {
            BuildingInfo.text = Player.Instance.CurrentBuilding.GetComponent<Building>().Description;
        }
    }

    void Sacrifice()
    {
        if (SelectedHuman != null && SelectedHuman.IsAlive())
            SelectedHuman.Sacrifice();
    }

    void EndGame()
    {
        SceneManager.LoadScene("stats", LoadSceneMode.Single);
    }

    void MassSacrifice()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            var hits = Physics.OverlapSphere(hit.point, massSacrificeRadius);
            foreach(var h in hits)
            {
                var human = h.GetComponent<Human>();
                if (human != null)
                    human.Sacrifice();
            }
        }
    }

    void UpdateMassSacrificeIndicator()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            massSacrificeIndicator.transform.position = hit.point;
            massSacrificeIndicator.transform.localScale = new Vector3(massSacrificeRadius, massSacrificeRadius, massSacrificeRadius);

            var rs = massSacrificeIndicator.GetComponentsInChildren<Renderer>();

            foreach (var renderer in rs)
            {
                var color = renderer.material.color;
                color.a = (massSacrificeHeld / massSacrificeHoldTime) * massSacrificeIndicatorMaxAlpha;
                renderer.material.color = color;
            }
        }

        massSacrificeIndicator.transform.Rotate(Vector3.up, Time.deltaTime * 50.0f);
    }

    void Kill()
    {
        if (SelectedHuman != null && SelectedHuman.IsAlive())
        {
            if (!SelectedHuman.IsAdult())
                SelectedHuman.GrowUp();
            SelectedHuman.BecomeOld();
            SelectedHuman.Die();
        }
    }

    void Spawn()
    {
        if (SelectedHuman != null && SelectedHuman.IsAlive() && SelectedHuman.IsAdult() && Player.Instance.Power >= 2)
        {
            SelectedHuman.GiveBirth(null);
            Player.Instance.Power -= 2;
        }
    }

    readonly float massSacrificeHoldTime = 2.0f;
    readonly float massSacrificeRadius = 1.0f;
    float massSacrificeHeld = 0.0f;
    bool massSacrificeInProgress = false;
    GameObject massSacrificeIndicator;
    float massSacrificeIndicatorMaxAlpha = 0.8f;
    float timeSinceGameEnd = 0.0f;
    readonly float VictoryTextScaleTime = 2.0f;
    readonly float VictoryTextFinalScale = 2.0f;
    bool endingGame = false;
}
