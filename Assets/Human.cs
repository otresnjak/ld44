﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum HumanState
{
    Idle,
    Afraid,
    InLove,
    Dead,
}

public class Human : MonoBehaviour
{
    public static int Population { get; private set; }
    public static int EnemyPopulation { get; private set; }

    public bool Enemy = false;
    public string FirstName;
    public string MiddleInitial;
    public string LastName;
    public GameObject BabyRoot;
    public GameObject AdultRoot;
    public GameObject[] SkinColorParts;
    public GameObject[] PantsParts;
    public GameObject[] ShirtParts;
    public GameObject[] HairParts;
    public GameObject WeaponRoot;
    public AnimationCurve HopCurve;
    public bool AdultAtSpawn = false;

    public GameObject SacrificeParticles;
    public GameObject SacrificeValuePrefab;
    public GameObject BonePrefab;
    public GameObject ArmedFloaterPrefab;
    public GameObject SplatterParticles;

    public GameObject InLoveFxPrefab;
    public GameObject DeathFxPrefab;

    public bool IsInLove()
    {
        return state == HumanState.InLove;
    }

    public bool IsAdult()
    {
        return AdultRoot.gameObject.activeSelf;
    }

    public bool IsArmed()
    {
        return armed;
    }

    public static void ResetStatics()
    {
        Population = EnemyPopulation = 0;
    }

    void Awake()
    {
        humanGenerator = FindObjectOfType<HumanGenerator>();
        body = GetComponent<Rigidbody>();
        AdultRoot.gameObject.SetActive(false);
        if (WeaponRoot != null)
            WeaponRoot.gameObject.SetActive(false);
    }

    void Start()
    {
        if (!Enemy)
            ++Population;
        else
            ++EnemyPopulation;

        if (Enemy)
            timeToHop = Random.Range(enemyHopIntervalRange.x, enemyHopIntervalRange.y);
        else
            timeToHop = Random.Range(hopIntervalRange.x, hopIntervalRange.y);

        if (AdultAtSpawn)
        {
            age = Random.Range(16.0f, 18.0f);
            GrowUp();
        }
        else
        {
            growUpAge = Random.Range(growUpAgeRange.x, growUpAgeRange.y);
            flavorText = string.Format("Child");
        }

        staticPathDestination = GetRandomNavMeshPos();
    }

    void Update()
    {
        pathAge += Time.deltaTime;

        timeToHop -= Time.deltaTime;
        if (timeToHop < 0.0f && !dying)
        {
            Hop();
        }

        if (!dying && !Enemy && IsAlive())
        {
            if (state == HumanState.Afraid)
            {
                if (fearTimeLeft <= 0.0f)
                {
                    state = HumanState.Idle;
                }
                fearTimeLeft -= Time.deltaTime;
            }

            if (age >= growUpAge && !IsAdult())
            {
                GrowUp();
            }

            if (IsAdult() && armed && EnemyPopulation > 0)
            {
                var targetHuman = pathDestination != null ? pathDestination.GetComponent<Human>() : null;

                // prioritize attacking enemies
                if (!targetHuman || !targetHuman.Enemy)
                    pathDestination = FindAttackTarget(2.5f, false);

                targetHuman = pathDestination != null ? pathDestination.GetComponent<Human>() : null;
                if (targetHuman != null && targetHuman.Enemy)
                {
                    if (Vector3.Distance(transform.position, pathDestination.position) < 0.25f)
                        targetHuman.Die(false, this);
                }

            }
            else if (IsAdult() && age >= loveAge && state == HumanState.Idle && Human.Population < Player.Instance.Housing)
            {
                FallInLove();
            }

            if (!old && IsAdult() && age > oldAge && (state == HumanState.Idle || state == HumanState.InLove))
            {
                BecomeOld();
            }

            if (IsAdult() && age > deathAge)
            {
                StatTracker.IncStat("Natural Deaths", 1);
                Die();
            }

            if (state == HumanState.InLove)
            {
                if (inLoveWith != null && Vector3.Distance(transform.position, inLoveWith.transform.position) < 0.25f)
                {
                    Instantiate(InLoveFxPrefab, transform.position + Vector3.up * 0.5f, Quaternion.identity);
                    Debug.Log(string.Format("{0} and {1} met!", GetName(), inLoveWith.GetName()));
                    state = HumanState.Idle;
                    loveAge = float.MaxValue;
                    inLoveWith.state = HumanState.Idle;
                    inLoveWith.loveAge = float.MaxValue;
                    pathDestination = null;
                    inLoveWith.pathDestination = null;
                    GiveBirth(inLoveWith);
                }
                else if (inLoveWith == null)
                {
                    Debug.Log(string.Format("{0}'s lover has died. {0} is pretty bummmed.", GetName(), GetName()));
                    flavorText = "Bereaved";
                    state = HumanState.Idle;
                    loveAge = float.MaxValue;
                }
            }

            if (staticPathDestination.HasValue && pathDestination == null && Vector3.Distance(transform.position, staticPathDestination.Value) < wayPointReachedDistance)
                staticPathDestination = GetRandomNavMeshPos();

            age += Time.deltaTime * agingRate;
        }

        if (Enemy && IsAlive())
        {
            if (toNextReprioritize > 0.0f)
                toNextReprioritize -= Time.deltaTime;

            if (pathDestination == null || toNextReprioritize < 0.0f)
            {
                pathDestination = FindAttackTarget(1.5f, false);
                if (pathDestination == null)
                    pathDestination = FindAttackTarget(10.0f, true);

                toNextReprioritize = reprioritizeInterval;

                if (pathDestination != null)
                    flavorText = "Attacking " + pathDestination.gameObject.name + "!";
                else
                    flavorText = "Triumphant";
            }

            if (pathDestination != null)
            {

                float threshold = 0.25f;
                if (pathDestination.GetComponent<Building>() != null)
                {
                    threshold = 0.5f;

                    var dist = Vector3.Distance(pathDestination.GetComponent<Building>().transform.position, transform.position);
                }

                if (Vector3.Distance(transform.position, pathDestination.position) < threshold)
                {
                    var targetHuman = pathDestination.GetComponent<Human>();
                    var targetBuilding = targetHuman == null ? pathDestination.GetComponentInParent<Building>() : null;

                    if (targetHuman != null && targetHuman.IsAlive())
                        targetHuman.Die(false, this);

                    if (targetBuilding != null)
                        targetBuilding.Damage(transform.position);
                }
            }

            if (IsAdult() && age > deathAge)
            {
                Die();
            }
        }

        if (transform.position.y < -5.0f)
        {
            if (!Enemy)
                --Population;
            else
                --EnemyPopulation;
            Destroy(gameObject);
        }
    }

    public void Arm()
    {
        Debug.Log(GetName() + " was armed by an armory!");
        Instantiate(ArmedFloaterPrefab, transform.position + Vector3.up * 0.4f, Quaternion.identity);
        WeaponRoot.SetActive(true);
        armed = true;
    }

    Transform FindAttackTarget(float attackRadius, bool useNearest)
    {
        var candidates = Physics.OverlapSphere(transform.position, attackRadius);

        Transform nearest = null;
        float nearestDist = float.MaxValue;

        foreach (var c in candidates)
        {
            Human h = c.GetComponent<Human>();
            if (h != null)
            {
                // don't attack humans of same faction
                if (Enemy && h.Enemy) h = null;
                if (!Enemy && !h.Enemy) h = null;
            }

            // only enemies attack buildings
            Building b = (h == null && Enemy) ? c.GetComponentInParent<Building>() : null;

            if (h != null || b != null)
            {
                if (!useNearest)
                    return b != null ? b.transform : c.transform;

                float dist = Vector3.Distance(transform.position, c.transform.position);
                if (dist < nearestDist)
                {
                    nearest = c.transform;
                    nearestDist = dist;
                }
            }
        }

        // if it's a building make sure we're attacking the root, not a piece of it
        if (nearest)
        {
            var bb = nearest.GetComponentInParent<Building>();
            if (bb) nearest = bb.transform;
        }

        return nearest;
    }

    void Hop()
    {
        float hopPowerH = 5.0f;
        float hopPowerV = 10.0f;
        Vector2 hopIntervalR = state == HumanState.Afraid ? afraidHopIntervalRange : hopIntervalRange;
        timeToHop = Enemy ? Random.Range(enemyHopIntervalRange.x, enemyHopIntervalRange.y) : Random.Range(hopIntervalR.x, hopIntervalR.y);

        var hopDirection = GetHopDirection();
        body.AddForce(hopDirection * hopPowerH + Vector3.up * hopPowerV);
    }

    public void GrowUp()
    {
        BabyRoot.SetActive(false);
        AdultRoot.SetActive(true);

        loveAge = Random.Range(loveAgeRange.x, loveAgeRange.y);
        oldAge = Random.Range(oldAgeRange.x, oldAgeRange.y);
        deathAge = Random.Range(deathAgeRange.x, deathAgeRange.y);

        flavorText = "Adult";
    }

    public void BecomeOld()
    {
        Debug.Log(string.Format("{0} grew old.", GetName()));
        old = true;
        flavorText = "Elderly";
        foreach (var part in HairParts)
            part.GetComponent<Renderer>().material.color = Color.white * 0.9f;
    }

    public void Die(bool naturalCauses = true, Human killer = null)
    {
        dying = true;

        if (naturalCauses && Random.Range(0.0f, 1.0f) < Player.Instance.GetGraveyardBonus())
        {
            int powerAmount = GetSacrificeValue();
            Player.Instance.Power += powerAmount;
            var valueFloater = Instantiate(SacrificeValuePrefab, transform.position + Vector3.up * 0.4f, Quaternion.identity);
            var valueText = valueFloater.GetComponentInChildren<TMPro.TMP_Text>();
            valueText.text = "+" + powerAmount;
        }

        if (naturalCauses)
        {
            Debug.Log(string.Format("{0} died of old age.", GetName()));
            flavorText = "Died of old age";
        }
        else
        {
            Debug.Log(string.Format("{0} was killed by {1}.", GetName(), killer.GetName()));
            flavorText = "Killed by " + killer.GetName();

            if (killer != null)
            {
                if (Enemy)
                    StatTracker.IncStat("Enemies Killed", 1);
                else
                    StatTracker.IncStat("Villagers Killed By Enemies", 1);
            }

            if (armed || Enemy)
                if (killer.IsAlive())
                    killer.Die(false, this);
        }

        body.constraints = 0;
        body.AddTorque(transform.forward * 5.0f);
        Instantiate(DeathFxPrefab, transform.position + Vector3.up * 0.5f, Quaternion.identity);

        Invoke("DestroyBody", 5.0f);
    }

    public bool IsAlive()
    {
        return !dying;
    }

    public void GiveBirth(Human otherParent)
    {
        if (!Enemy)
        {
            int numBabies = Random.Range(1, 3);

            float templeBonus = Player.Instance.GetTempleBonus();
            if (Random.Range(0.0f, 1.0f) < templeBonus)
                numBabies += 1;

            for (int i = 0; i < numBabies; ++i)
            {
                if (otherParent != null)
                    StatTracker.IncStat("Babies Born Naturally", 1);
                else
                    StatTracker.IncStat("Babies Born Through Evil Means", 1);

                var babyPos = transform.position + Util.FlattenY(transform.position + Random.onUnitSphere).normalized * 0.2f;
                babyPos = PlayableArea.ClampToPlayableArea(babyPos);
                if (otherParent != null)
                {
                    var baby = humanGenerator.CreateHuman(false, babyPos, this, otherParent);
                    flavorText = string.Format("Parent of {0} (with {1})", baby.GetName(), otherParent.GetName());
                    otherParent.flavorText = string.Format("Parent of {0} (with {1})", baby.GetName(), GetName());
                }
                else
                {
                    var baby = humanGenerator.CreateHuman(false, babyPos);
                    flavorText = string.Format("Parent of {0} (with DARK GOD)", baby.GetName());
                    Instantiate(SacrificeParticles, baby.transform.position, baby.transform.rotation);
                }
            }
        }
    }

    void FallInLove()
    {
        float loveRadius = 10.0f;
        var candidates = Physics.OverlapSphere(transform.position, loveRadius);
        foreach (var c in candidates)
        {
            Human h = c.GetComponent<Human>();
            if (h && !h.Enemy && h.IsAdult() && !h.IsInLove() && LastName != h.LastName)
            {
                inLoveWith = h;
                h.inLoveWith = this;
                state = HumanState.InLove;
                h.state = HumanState.InLove;
                flavorText = "In love with " + h.GetName();
                h.flavorText = "In love with " + GetName();
                Debug.Log(string.Format("{0} and {1} have fallen in love!", GetName(), h.GetName()));
                pathDestination = inLoveWith.transform;
                inLoveWith.pathDestination = transform;
                break;
            }
        }
    }

    public string GetName()
    {
        if (MiddleInitial == "")
            return FirstName + " " + LastName;
        else
            return FirstName + " " + MiddleInitial + " " + LastName;
    }

    public int GetAge()
    {
        return Mathf.RoundToInt(age);
    }

    public string GetFlavorText()
    {
        return flavorText;
    }

    public void Terrify(Vector3 src)
    {
        fearSource = src;
        state = HumanState.Afraid;
        fearTimeLeft = fearDuration;
        timeToHop = 0.2f;
    }

    void OnMouseDown()
    {
        if (!dying)
            HUD.Instance.SelectedHuman = this;
    }

    int GetSacrificeValue()
    {
        int powerAmount = 1;
        float statueBonus = Player.Instance.GetStatueBonus();
        if (Random.Range(0.0f, 1.0f) < statueBonus)
            powerAmount += 1;
        return powerAmount;
    }

    public void Sacrifice(bool force = false)
    {
        if (!Enemy || force)
        {
            if (!Enemy)
            {
                int powerAmount = GetSacrificeValue();
                Player.Instance.Power += powerAmount;
                var valueFloater = Instantiate(SacrificeValuePrefab, transform.position + Vector3.up * 0.4f, Quaternion.identity);
                var valueText = valueFloater.GetComponentInChildren<TMPro.TMP_Text>();
                valueText.text = "+" + powerAmount;

                StatTracker.IncStat("Humans Sacrificed", 1);
                if (!IsAdult())
                    StatTracker.IncStat("Children Sacrificed (you monster)", 1);
            }

            Instantiate(SacrificeParticles, transform.position, transform.rotation);
            dying = true;
            Destroy(body);

            Invoke("TerrifyNeighbors", 0.15f);
            Invoke("SpawnBonesExplode", 0.1f);
            Invoke("DeleteSelf", 0.2f);
        }
    }

    public void Splatter()
    {
        StatTracker.IncStat("Enemies Killed", 1);
        StatTracker.IncStat("Enemies Killed By Towers", 1);
        Instantiate(SplatterParticles, transform.position, transform.rotation);
        dying = true;
        Destroy(body);

        Invoke("SpawnBonesExplode", 0.1f);
        Invoke("DeleteSelf", 0.2f);
    }

    public void DestroyBody()
    {
        SpawnBones(false);
        DeleteSelf();
    }

    void TerrifyNeighbors()
    {
        float fearRadius = 1.0f;
        var overlapResult = Physics.OverlapSphere(transform.position, fearRadius);
        foreach (var c in overlapResult)
        {
            var h = c.GetComponent<Human>();
            if (h != null && h != this)
            {
                h.Terrify(transform.position);
            }
        }
    }

    void DeleteSelf()
    {
        if (!Enemy)
            --Population;
        else
            --EnemyPopulation;
        Destroy(gameObject);
    }

    void SpawnBonesExplode()
    {
        SpawnBones(true);
    }

    void SpawnBones(bool explode = true)
    {
        for (int i = 0; i < 4; ++i)
        {
            SpawnBone(explode);
        }
    }

    void SpawnBone(bool explode)
    {
        var bone = Instantiate(BonePrefab);
        bone.transform.position = transform.position + Vector3.up * Random.Range(0.05f, 0.15f);

        Vector3 upForce;
        if (explode)
            upForce = Vector3.up * 6.0f;
        else
            upForce = Vector3.up;

        bone.GetComponent<Rigidbody>().AddForce(upForce + Util.FlattenY(Random.insideUnitSphere.normalized) * 1.2f);
        bone.GetComponent<Rigidbody>().AddTorque(Random.insideUnitSphere.normalized * 2.0f);
    }

    void UpdatePath()
    {
        if (pathDestination != null || staticPathDestination.HasValue)
        {
            currentWaypoint = 0;
            path = new NavMeshPath();

            var destPos = pathDestination != null ? pathDestination.transform.position : staticPathDestination.Value;

            // find the nearest point to destPos on the actual nav mesh - this should guarantee we don't end up trying to
            // path to a point off the navmesh (for example inside a obstacle that cuts it, e.g. a building) and failing
            if (pathDestination != null)
            {
                NavMeshHit hit;
                NavMesh.SamplePosition(destPos, out hit, 5.0f, NavMesh.AllAreas);
                destPos = hit.position;
            }

            Vector3 dest = Util.FlattenY(destPos);
            NavMesh.CalculatePath(transform.position, dest, NavMesh.AllAreas, path);

            if (path.status == NavMeshPathStatus.PathInvalid)
            {
                // we can't reach this destination so let's look for something else
                pathDestination = null;
                staticPathDestination = null;
            }
        }
        else
        {
            ClearPath();
        }
    }

    void ClearPath()
    {
        path = null;
    }

    Vector3 GetNextWaypointDirection()
    {
        if (path == null || pathAge > repathInterval)
            UpdatePath();

        // UpdatePath may have failed, so just do a random hop until we get a new path
        if (pathDestination == null && !staticPathDestination.HasValue)
            return Random.onUnitSphere;

        Debug.Assert(path != null);

        var waypoint = path.corners[currentWaypoint];
        if (Vector3.Distance(waypoint, transform.position) < wayPointReachedDistance)
        {
            currentWaypoint += 1;
            if (currentWaypoint < path.corners.Length)
                waypoint = path.corners[currentWaypoint];
            else
            {
                // reached destination
                staticPathDestination = null;
                path = null;
            }
        }

        return waypoint - transform.position;
    }

    Vector3 GetHopDirection()
    {
        Vector3 hopDirection = new Vector3();
        if (state == HumanState.Afraid)
            hopDirection = transform.position - fearSource;
        else if (pathDestination != null || staticPathDestination.HasValue)
            hopDirection = GetNextWaypointDirection();
        else
            hopDirection = Random.onUnitSphere;
        hopDirection = Util.FlattenY(hopDirection);
        hopDirection = hopDirection.normalized;
        return hopDirection;
    }

    Vector3 GetRandomNavMeshPos()
    {
        float maxDistance = 2.0f;
        Vector3 origin = transform.position;
        Vector3 samplePt = origin + Util.FlattenY(Random.insideUnitSphere).normalized * maxDistance;
        NavMeshHit hit;
        NavMesh.SamplePosition(samplePt, out hit, maxDistance, NavMesh.AllAreas);
        return hit.position;
    }

    Transform pathDestination;
    Vector3? staticPathDestination;
    NavMeshPath path;
    float wayPointReachedDistance = 0.25f;
    int currentWaypoint = 0;
    float pathAge = 0.0f;
    float repathInterval = 1.0f;
    float reprioritizeInterval = 5.0f;
    float toNextReprioritize;

    readonly float agingRate = 0.8f;

    bool dying = false;
    Vector2 hopIntervalRange = new Vector2(1.0f, 2.0f);
    Vector2 enemyHopIntervalRange = new Vector2(0.5f, 1.5f);
    Vector2 afraidHopIntervalRange = new Vector2(0.4f, 0.6f);
    float timeToHop;
    Rigidbody body;
    HumanState state = HumanState.Idle;
    Vector3 fearSource;
    float fearDuration = 3.0f;
    float fearTimeLeft;

    Human inLoveWith;
    Vector2 loveAgeRange = new Vector2(15.0f, 35.0f);
    float loveAge;

    float age = 0.0f;

    Vector2 growUpAgeRange = new Vector2(16.0f, 18.0f);
    float growUpAge;

    Vector2 oldAgeRange = new Vector2(60.0f, 80.0f);
    float oldAge;

    Vector2 deathAgeRange = new Vector2(75.0f, 90.0f);
    float deathAge;

    string flavorText = "";

    bool old = false;
    bool parent = false;
    Human child = null;

    bool armed = false;

    HumanGenerator humanGenerator;
}
