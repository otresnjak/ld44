﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanGenerator : MonoBehaviour
{
    public static HumanGenerator Instance { get; private set; }

    public string[] FirstNames;
    public string[] LastNames;
    public string[] EnemyTitles;
    public Color[] SkinColors;
    public Human HumanPrefab;
    public Human EnemyHumanPrefab;
    public Transform EnemySpawnPoint;
    public int StartingHumans = 100;
    public int DebugStartingEnemies = 0;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Human.ResetStatics();
        for (int i = 0; i < StartingHumans; ++i)
            CreateHuman(true);
        for (int i = 0; i < DebugStartingEnemies; ++i)
            CreateEnemyHuman();
    }

    void Update()
    {
        //toNextSpawn -= Time.deltaTime;
        //if (toNextSpawn < 0.0f)
        //{
        //    toNextSpawn = spawnInterval;
        //    CreateHuman();
        //}
    }

    public Human CreateHuman(bool adult, Vector3? position = null, Human parent0 = null, Human parent1 = null)
    {
        var h = Instantiate<Human>(HumanPrefab);
        h.AdultAtSpawn = adult;
        CreateName(h, parent0, parent1);
        ColorPants(h);
        ColorShirt(h);
        ColorSkin(h);
        ColorHat(h);

        Vector3 pos;
        if (position.HasValue)
        {
            pos = position.Value;
        }
        else
        {
            pos = new Vector3();
            pos.x = Random.Range(-3.0f, 3.0f);
            pos.y = 0.0f;
            pos.z = Random.Range(-3.0f, 3.0f);
        }

        h.transform.position = pos;

        return h;
    }

    public Human CreateEnemyHuman(Vector3? position = null)
    {
        var h = Instantiate<Human>(EnemyHumanPrefab);
        h.Enemy = true;
        h.AdultAtSpawn = true;
        CreateEnemyName(h);
        ColorSkin(h);
        ColorHat(h);

        Vector3 pos;
        if (position.HasValue)
        {
            pos = position.Value;
        }
        else
        {
            pos = EnemySpawnPoint.transform.position;
        }

        h.transform.position = pos;

        return h;
    }

    void CreateName(Human h, Human parent0 = null, Human parent1 = null)
    {
        bool hasMI = Random.Range(0.0f, 1.0f) < 0.2f;
        h.FirstName = FirstNames[Random.Range(0, FirstNames.Length)];
        if (hasMI) h.MiddleInitial = ((char)('A' + Random.Range(0, 26))).ToString() + ".";

        if (parent0 == null || Random.Range(0.0f, 1.0f) < 0.25f)    // ignore parent names and choose a new one sometimes (so we don't gradually run out of names)
        {
            h.LastName = LastNames[Random.Range(0, LastNames.Length)];
        }
        else
        {
            float result = Random.Range(0.0f, 1.0f);
            if (result < 0.2f && !parent0.LastName.Contains("-") && !parent1.LastName.Contains("-"))
                h.LastName = parent0.LastName + "-" + parent1.LastName;
            else
            {
                if (result <= 0.5f)
                    h.LastName = parent0.LastName;
                else
                    h.LastName = parent1.LastName;
            }
        }

        h.gameObject.name = h.GetName();
    }

    void CreateEnemyName(Human h)
    {
        h.FirstName = FirstNames[Random.Range(0, FirstNames.Length)];
        h.LastName = EnemyTitles[Random.Range(0, EnemyTitles.Length)];
        h.gameObject.name = h.GetName();
    }

    void ColorSkin(Human h)
    {
        var color = SkinColors[Random.Range(0, SkinColors.Length)];
        ColorSection(h.SkinColorParts, color);
    }

    void ColorShirt(Human h)
    {
        var color = Random.ColorHSV(0.0f, 1.0f, 0.1f, 0.6f, 0.25f, 1.0f);
        ColorSection(h.ShirtParts, color);
    }

    void ColorPants(Human h)
    {
        var color = Random.ColorHSV(0.0f, 1.0f, 0.1f, 0.6f, 0.25f, 1.0f);
        ColorSection(h.PantsParts, color);
    }

    void ColorHat(Human h)
    {
        var color = Random.ColorHSV(0.0f, 1.0f, 0.1f, 0.6f, 0.25f, 1.0f);
        ColorSection(h.HairParts, color);
    }

    void ColorSection(GameObject[] section, Color color)
    {
        foreach (var p in section)
        {
            p.GetComponent<Renderer>().material.color = color;
        }
    }

    //float spawnInterval = 0.1f;
    //float toNextSpawn = 0.0f;
}
