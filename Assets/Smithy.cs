﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smithy : MonoBehaviour
{
    public float ArmRadius = 2.0f;
    public Vector2 ArmIntervalRange = new Vector2(30.0f, 90.0f);
    public GameObject ArmRadiusIndicator;


    void Start()
    {
        var scale = ArmRadiusIndicator.transform.localScale;
        scale.x = scale.z = ArmRadius;
        ArmRadiusIndicator.transform.localScale = scale;
        timeToNextArm = Random.Range(ArmIntervalRange.x, ArmIntervalRange.y);
    }

    void Update()
    {
        if (timeToNextArm <= 0.0f)
        {
            var candidates = Physics.OverlapSphere(transform.position, ArmRadius);
            foreach(var c in candidates)
            {
                Human h = c.GetComponent<Human>();
                if (h != null && !h.Enemy && h.IsAdult() && !h.IsArmed())
                {
                    h.Arm();
                    break;
                }
            }

            timeToNextArm = Random.Range(ArmIntervalRange.x, ArmIntervalRange.y);
        }

        timeToNextArm -= Time.deltaTime;
    }

    void Arm()
    {

    }

    float timeToNextArm;
}
