﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaveSpawner : MonoBehaviour
{
    public static EnemyWaveSpawner Instance { get; private set; }

    public float[] WaveTimes;
    public int[] WaveSizes;
    public float TimeToNextWave
    {
        get
        {
            if (nextWave < WaveTimes.Length)
                return Mathf.Max(0.0f, WaveTimes[nextWave] - age);
            else
                return -1;
        }
    }

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Debug.Assert(WaveSizes.Length == WaveTimes.Length);
    }

    void Update()
    {
        age += Time.deltaTime;

        if (nextWave < WaveTimes.Length)
        {
            if (age >= WaveTimes[nextWave])
            {
                for (int i = 0; i < WaveSizes[nextWave]; ++i)
                    HumanGenerator.Instance.CreateEnemyHuman();
                ++nextWave;
            }
        }
    }

    int nextWave = 0;
    float age = 0.0f;
}
