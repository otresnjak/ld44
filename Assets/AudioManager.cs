﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance { get; private set; }
    public AudioClip PeacefulSong;
    public AudioClip BattleSong;

    void Awake()
    {
        Instance = this;
        source = GetComponent<AudioSource>();
    }

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Human.EnemyPopulation > 0)
        {
            SetSong(BattleSong);
        }
        else
        {
            SetSong(PeacefulSong);
        }
    }

    void SetSong(AudioClip song)
    {
        if (source.clip != song)
        {
            source.clip = song;
            source.Play();
        }
    }

    AudioSource source;
}
