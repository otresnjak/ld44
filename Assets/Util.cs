﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util
{
    public static Vector3 FlattenY(Vector3 v)
    {
        return new Vector3(v.x, 0.0f, v.z);
    }
}
