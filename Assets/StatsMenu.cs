﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StatsMenu : MonoBehaviour
{
    public TMPro.TMP_Text StatsText;

    void Start()
    {
        StatsText.text = "";
        foreach (var stat in StatTracker.Stats)
        {
            string entry = stat.Key + ": <color=\"red\">" + stat.Value + "</color>\n";
            StatsText.text += entry;
        }
    }

    public void ReturnToMainMenu()
    {
        StatTracker.Clear();
        SceneManager.LoadScene("startup", LoadSceneMode.Single);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
