﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour
{
    void Start()
    {
        
    }

    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        var position = transform.position;
        position.x += -v * Time.deltaTime * moveRate;
        position.z += h * Time.deltaTime * moveRate;

        if (position.x < -5) position.x = -5;
        if (position.x > 10) position.x = 10;
        if (position.z < -8) position.z = -8;
        if (position.z > 6) position.z = 6;

        transform.position = position;
    }

    float moveRate = 2.0f;
}
