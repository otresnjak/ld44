﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player Instance { get; private set; }

    public int Power;

    public int HouseCost = 1;
    public int Housing = 0;
    public int Statues = 0;
    public int Temples = 0;
    public int Graveyards = 0;
    public GameObject HousePrefab;
    public GameObject StatuePrefab;
    public GameObject TemplePrefab;
    public GameObject GraveyardPrefab;
    public GameObject SmithyPrefab;
    public GameObject TowerPrefab;
    public GameObject AltarPrefab;

    public GameObject CurrentBuilding;

    float playTime = 0.0f;

    public bool Win { get; set; }
    bool lost = false;

    public bool Won()
    {
        return Win;
    }

    public bool Lost()
    {
        return !Win && lost;
    }

    void Awake()
    {
        Instance = this;
        Win = false;
    }

    void Start()
    {
        CurrentBuilding = HousePrefab;
    }

    void Update()
    {
        playTime += Time.deltaTime;
        StatTracker.SetStat("Play Time", Mathf.RoundToInt(playTime));
        if (CurrentBuilding != null)
        {
            var bdPrefab = CurrentBuilding.GetComponent<Building>();
            int cost = bdPrefab.BuildCost;
            if (Input.GetKeyDown(KeyCode.Mouse1) && Power >= cost)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.point.y <= 0.02f)        // avoid placing things on top of other things
                    {
                        var spawnPos = hit.point;
                        Debug.Log("spawnPos = " + spawnPos);

                        if (PlayableArea.IsInPlayableArea(spawnPos))
                        {
                            StatTracker.IncStat("Buildings Built", 1);

                            Power -= cost;
                            spawnPos.y += bdPrefab.SpawnHeight;
                            var obj = GameObject.Instantiate(CurrentBuilding, spawnPos, Quaternion.identity);

                            var building = obj.GetComponent<Building>();
                        }
                    }
                }
            }
        }
        else
        {
            Debug.LogWarning("currentBuilding is null");
        }

        // checking playtime avoids a weird initialization order bug (?) that only happens in builds, not in the editor
        if (playTime > 5.0f && Human.Population <= 0 && !Win)
            lost = true;
    }

    public void ChooseHouse()
    {
        CurrentBuilding = HousePrefab;
    }

    public void ChooseStatue()
    {
        CurrentBuilding = StatuePrefab;
    }

    public void ChooseTemple()
    {
        CurrentBuilding = TemplePrefab;
    }

    public void ChooseGraveyard()
    {
        CurrentBuilding = GraveyardPrefab;
    }

    public void ChooseSmithy()
    {
        CurrentBuilding = SmithyPrefab;
    }

    public void ChooseAltar()
    {
        CurrentBuilding = AltarPrefab;
    }

    public void ChooseTower()
    {
        CurrentBuilding = TowerPrefab;
    }

    public float GetTempleBonus()
    {
        return Temples * 0.1f;
    }

    public float GetStatueBonus()
    {
        return Statues * 0.1f;
    }

    public float GetGraveyardBonus()
    {
        return Graveyards * 0.1f;
    }
}
