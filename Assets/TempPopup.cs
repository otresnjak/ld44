﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempPopup : MonoBehaviour
{
    public float Lifespan = 3.0f;
    public AnimationCurve AlphaCurve;
    public Vector3 Velocity = new Vector3(0.0f, 0.2f, 0.0f);

    void Awake()
    {
        render = GetComponent<Renderer>();
    }

    void Update()
    {
        if (Lifespan >= age)
        {
            //Destroy(gameObject);
        }

        if (render.material.HasProperty("_Color"))
        {
            var color = render.material.GetColor("_Color");
            color.a = AlphaCurve.Evaluate(age / Lifespan);
            render.material.SetColor("_Color", color);
        }

        age += Time.deltaTime;

        var pos = transform.position;
        pos += Velocity * Time.deltaTime;
        transform.position = pos;
    }

    float age = 0.0f;
    Renderer render;
}
