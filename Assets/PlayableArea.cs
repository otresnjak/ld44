﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableArea : MonoBehaviour
{
    public Transform Min;
    public Transform Max;

    public static PlayableArea Instance { get; private set; }

    void Awake()
    {
        Instance = this;
    }

    public static Vector3 ClampToPlayableArea(Vector3 oldPos)
    {
        Vector3 pos = oldPos;

        if (pos.x < Instance.min.x)
            pos.x = Instance.min.x;
        if (pos.z < Instance.min.z)
            pos.z = Instance.min.z;

        if (pos.x > Instance.max.x)
            pos.x = Instance.max.x;
        if (pos.z > Instance.max.z)
            pos.z = Instance.max.z;

        return pos;
    }

    public static bool IsInPlayableArea(Vector3 pos)
    {
        if (pos.x < Instance.min.x)
            return false;
        if (pos.z < Instance.min.z)
            return false;

        if (pos.x > Instance.max.x)
            return false;
        if (pos.z > Instance.max.z)
            return false;

        return true;
    }

    void Start()
    {
        min = Min.position;
        max = Max.position;
    }

    Vector3 min, max;
}
